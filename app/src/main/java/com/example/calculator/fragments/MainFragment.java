package com.example.calculator.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.calculator.R;
import com.example.calculator.debug.Debug;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 * User: Pavel
 * Date: 21.09.2015
 * Time: 14:04
 */
public class MainFragment extends Fragment implements View.OnClickListener, View.OnLongClickListener {

    /*************************************
     * PRIVATE STATIC CONSTANTS
     *************************************/
    private static final String TAG = MainFragment.class.getSimpleName();

    /*************************************
     * PUBLIC STATIC CONSTANTS
     *************************************/
    public static final String INFINITY_0 = "Infinity";
    public static final String INFINITY_1 = "-Infinity";

    /*************************************
     * PRIVATE FIELDS
     *************************************/
    private Button[] numberButtons = new Button[10];
    private Button actionButtonPlus;
    private Button actionButtonMinus;
    private Button actionButtonMultiply;
    private Button actionButtonDivide;
    private Button actionButtonClear;
    private Button actionButtonDelete;
    private Button actionButtonCalculate;
    private Button symbolButtonDot;
    private TextView screen;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        RelativeLayout rootView = (RelativeLayout) inflater.inflate(R.layout.fragment_main, null);

        /**
         * Pavel:
         * buttons can be created different ways
         * For example I can create list of buttons and list of values
         * then each button will not have special id and onclick method
         * will not have such long switch block
         *
         * but I prefer do like that, prefer readability then very short code.
         * Also then we can change behavior of some particular button easily
         */

        numberButtons[0] = (Button) rootView.findViewById(R.id.number_zero);
        numberButtons[1] = (Button) rootView.findViewById(R.id.number_one);
        numberButtons[2] = (Button) rootView.findViewById(R.id.number_two);
        numberButtons[3] = (Button) rootView.findViewById(R.id.number_three);
        numberButtons[4] = (Button) rootView.findViewById(R.id.number_four);
        numberButtons[5] = (Button) rootView.findViewById(R.id.number_five);
        numberButtons[6] = (Button) rootView.findViewById(R.id.number_six);
        numberButtons[7] = (Button) rootView.findViewById(R.id.number_seven);
        numberButtons[8] = (Button) rootView.findViewById(R.id.number_eight);
        numberButtons[9] = (Button) rootView.findViewById(R.id.number_nine);

        for (Button b : numberButtons) {
            b.setOnClickListener(this);
        }

        actionButtonPlus = (Button) rootView.findViewById(R.id.action_plus);
        actionButtonMinus = (Button) rootView.findViewById(R.id.action_minus);
        actionButtonMultiply = (Button) rootView.findViewById(R.id.action_multiply);
        actionButtonDivide = (Button) rootView.findViewById(R.id.action_divide);
        actionButtonDelete = (Button) rootView.findViewById(R.id.action_delete);
        actionButtonCalculate = (Button) rootView.findViewById(R.id.action_calculate);

        actionButtonPlus.setOnClickListener(this);
        actionButtonMinus.setOnClickListener(this);
        actionButtonMultiply.setOnClickListener(this);
        actionButtonDivide.setOnClickListener(this);
        actionButtonDelete.setOnClickListener(this);
        actionButtonDelete.setOnLongClickListener(this);
        actionButtonCalculate.setOnClickListener(this);

        symbolButtonDot = (Button) rootView.findViewById(R.id.symbol_dot);
        symbolButtonDot.setOnClickListener(this);

        screen = (TextView) rootView.findViewById(R.id.screen);

        return rootView;

    }

    @Override
    public void onClick(View view) {
        // Quite simple switch block when we choose what to do on click
        switch (view.getId()) {
            case R.id.number_one:
            case R.id.number_two:
            case R.id.number_three:
            case R.id.number_four:
            case R.id.number_five:
            case R.id.number_six:
            case R.id.number_seven:
            case R.id.number_eight:
            case R.id.number_nine:
            case R.id.number_zero:
            case R.id.symbol_dot:
            case R.id.action_plus:
            case R.id.action_minus:
            case R.id.action_divide:
            case R.id.action_multiply:
                appendSymbolToScreen(((Button) view).getText().toString());
                break;
            case R.id.action_calculate:
                calculateResult();
                break;
            case R.id.action_delete:
                deleteLastSymbolFromTheScreen();
                break;
        }
    }

    @Override
    public boolean onLongClick(View view) {
        // Clear screen on long click on delete button
        screen.setText("");
        return false;
    }

    /*************************************
     * PRIVATE METHODS
     *************************************/
    private void appendSymbolToScreen(String symbol) {
        Debug.log(TAG, "appendSymbolToScreen");


        checkForInfinity();

        // Replace last symbol (if it action) to new action
        if (symbolIsActionOrDot(symbol)) {
            trimActionSymbolsFromScreen();
        }

        // Avoid printing two dots one after other
        if (symbol.equals(getString(R.string.symbol_dot)) && lastNumberContainsDot()) {
            return;
        }

        screen.setText(screen.getText() + symbol);

    }

    private void deleteLastSymbolFromTheScreen() {
        Debug.log(TAG, "deleteLastSymbolFromTheScreen");
        String currentText = screen.getText().toString();
        if (currentText.length() > 0) {
            screen.setText(currentText.substring(0, currentText.length() - 1));
        }
    }

    private void deleteFirstSymbolFromTheScreen() {
        Debug.log(TAG, "deleteLastSymbolFromTheScreen");
        String currentText = screen.getText().toString();
        if (currentText.length() > 0) {
            screen.setText(currentText.substring(1, currentText.length()));
        }
    }

    private void calculateResult() {
        Debug.log(TAG, "calculateResult");

        // Remove "Infinity" word from the screen to avoid crashes
        checkForInfinity();

        // Trim action symbols before calculating
        trimActionSymbolsFromScreen();

        if (screen.getText().toString().length() > 0) {
            // Catch arithmetic exceptions like division by zero.
            try {
                // Using special library to parse expression and calculate it
                Expression e = new ExpressionBuilder(screen.getText().toString()).build();
                double result = e.evaluate();
                Debug.log(TAG, "calculateResult - result - " + result);
                screen.setText(trimZerosAfterDot(String.valueOf(result)));
            } catch (ArithmeticException e) {
                Debug.log(TAG, "Exception - " + Log.getStackTraceString(e));
                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }

    private void trimActionSymbolsFromScreen() {
        Debug.log(TAG, "trimActionSymbolsFromScreen");
        if (symbolIsActionOrDot(getLastScreenCharacter())) {
            deleteLastSymbolFromTheScreen();
        }
        if (symbolIsActionOrDot(getFirstScreenCharacter())) {
            deleteFirstSymbolFromTheScreen();
        }
    }

    private void checkForInfinity() {
        Debug.log(TAG, "checkForInfinity");
        screen.setText(screen.getText().toString().replace(INFINITY_0, ""));
        screen.setText(screen.getText().toString().replace(INFINITY_1, ""));
    }

    private String trimZerosAfterDot(String string) {
        Debug.log(TAG, "trimZerosAfterDot");

        // if there are no dots - just return
        if (!string.contains(getString(R.string.symbol_dot))) {
            return string;
        }

        // remove all zeros from the end
        while (getLastStringCharacter(string).equals(getString(R.string.number_zero))) {
            string = string.substring(0, string.length() - 1);
        }

        // remove dot, if this is the last character
        if (getLastStringCharacter(string).equals(getString(R.string.symbol_dot))) {
            string = string.substring(0, string.length() - 1);
        }

        return string;
    }

    private String getLastScreenCharacter() {
        Debug.log(TAG, "getLastScreenCharacter");
        if (screen.getText().toString().length() > 0) {
            return getLastStringCharacter(screen.getText().toString());
        }
        return "";
    }

    private String getFirstScreenCharacter() {
        Debug.log(TAG, "getFirstScreenCharacter");
        if (screen.getText().toString().length() > 0) {
            return getFirstStringCharacter(screen.getText().toString());
        }
        return "";
    }

    private String getLastStringCharacter(String s) {
        Debug.log(TAG, "getLastStringCharacter");
        return s.substring(s.length() - 1);
    }

    private String getFirstStringCharacter(String s) {
        Debug.log(TAG, "getFirstStringCharacter");
        return s.substring(0, 1);
    }

    private boolean symbolIsActionOrDot(String symbol) {
        Debug.log(TAG, "symbolIsActionOrDot");

        return symbol.equals(getString(R.string.action_plus)) ||
                symbol.equals(getString(R.string.action_minus)) ||
                symbol.equals(getString(R.string.action_multiply)) ||
                symbol.equals(getString(R.string.action_divide)) ||
                symbol.equals(getString(R.string.symbol_dot));
    }

    private boolean lastNumberContainsDot() {
        Debug.log(TAG, "lastNumberContainsDot");

        // Return if there are no dots
        if (!screen.getText().toString().contains(getString(R.string.symbol_dot))) {
            return false;
        }

        // Split all expression by a dots and check last element of the array.
        // If there are an actions symbols --> last number not contains dot, and otherwise
        String[] array = screen.getText().toString().split("\\" + getString(R.string.symbol_dot));
        return !(array[array.length - 1].contains(getString(R.string.action_plus)) ||
                array[array.length - 1].contains(getString(R.string.action_minus)) ||
                array[array.length - 1].contains(getString(R.string.action_divide)) ||
                array[array.length - 1].contains(getString(R.string.action_multiply)));

    }

}
