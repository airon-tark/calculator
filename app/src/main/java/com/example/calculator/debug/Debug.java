package com.example.calculator.debug;

import android.app.ActivityManager;
import android.os.Build;
import android.util.Log;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Vector;

public class Debug {

    /**
     * This is just util class for logging
     */

    private static final String TAG = Debug.class.getSimpleName();

    public static boolean enabled = false;

    private static MemoryThread memoryThread = null;
    private static Vector<Throwable> keptThrowables = new Vector<Throwable>();
    public static final int LOG_STRING_LENGTH = 30;

    public static void log(String msg) {
        if (!enabled) {
            return;
        }

        log("", msg, (Throwable) null);
    }

    public static void log(String msg, Throwable t) {
        if (!enabled) {
            return;
        }
        /**
         * TODO
         * Solve this later
         */
        //log(Common.LOG_TAG_DEBUG, msg, t);
    }

    public static void log(String tag, String message) {
        if (!enabled) {
            return;
        }
        log(tag, message, (Throwable) null);
    }

    public static void log(String tag, String message, String... params) {
    }

    public static void log(String tag, String msg, Throwable t) {
        if (!enabled) {
            return;
        }

        if (t != null) {
            Log.e(tag, msg, t);
            t.printStackTrace();
        } else {
            //Log.e(increaseToManySymbols(tag), String.format("%s - %s", Thread.currentThread().getId(), msg));
            Log.e(increaseToManySymbols(tag), msg);
        }
    }

    public static void logToFile(String tag, String message) {
        /**
         * add line to log file
         */
    }

    public static void logStackTrace(String tag, String message) {

        if (!enabled) {
            return;
        }

        StackTraceElement[] traces = Thread.currentThread().getStackTrace();

        message += "\n...";

        for (int i = 4; i < 8; i++) {
            String className = traces[i].getClassName();
            int length = className.split("\\.").length;
            if (className.contains(".") && length != 0) {
                message += "\n" + className.split("\\.")[length - 1] + "." + traces[i].getMethodName() + "()";
            } else {
                message += "\n" + className + "." + traces[i].getMethodName() + "()";
            }
        }
        message += "\n...";

        log(tag, message, (Throwable) null);

    }

    public static String getMemoryUsage() {
        return getMemoryUsage("\n");
    }

    public static String getMemoryUsage(String sep) {
        DecimalFormat format = new DecimalFormat();

        ActivityManager.MemoryInfo info = new ActivityManager.MemoryInfo();

        return "Memory" + sep +
                "--------" + sep +
                /**
                 * TODO
                 * Solve this later
                 */
                // "Utils:         " + format.format(Utils.availableMemory()) + sep +
                "Runtime Total: " + format.format(Runtime.getRuntime().totalMemory()) + sep +
                "Runtime Using: " + format.format((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())) + sep +
                "Runtime Free:  " + format.format(Runtime.getRuntime().freeMemory()) + sep +
                "Runtime Max:   " + format.format(Runtime.getRuntime().maxMemory()) + sep;
    }

    public static void logMemoryUsage() {
        log(getMemoryUsage());
    }

    public static void startFreeMemoryNotificationThread(long delay) {
        if (memoryThread != null)
            memoryThread.stopThread();

        memoryThread = new MemoryThread(delay);
        memoryThread.start();
    }

    public static void stopFreeMemoryNotificationThread() {
        if (memoryThread != null)
            memoryThread.stopThread();
    }

    public static void keepThrowable(Throwable t) {
        keptThrowables.add(0, t);
    }

    public static Vector<Throwable> getKeptThrowables() {
        return keptThrowables;
    }

    public static void clearKeptThrowables() {
        keptThrowables.clear();
    }

    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic");
    }

    // ---

    private static String increaseToManySymbols(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(".");
        for (int i = 0; i < (LOG_STRING_LENGTH - s.length()); i++) {
            sb.append("\u0020");
        }
        sb.append(s);
        return new String(sb);
    }

    // ---

    private static class MemoryThread extends Thread {

        private long delay = 5000;
        private boolean running = false;

        public MemoryThread(long delay) {
            this.delay = delay;
        }

        public void stopThread() {
            running = false;
        }

        public void run() {
            running = true;
            DecimalFormat format = new DecimalFormat();

            while (running) {
                log("MEMORY", new Date() + " - Free Memory - " + format.format(Runtime.getRuntime().freeMemory()), (Throwable) null);
                try {
                    Thread.sleep(delay);
                } catch (Exception e) {
                }
            }
        }
    }
}
