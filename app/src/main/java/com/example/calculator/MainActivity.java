package com.example.calculator;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import com.example.calculator.debug.Debug;
import com.example.calculator.fragments.MainFragment;


public class MainActivity extends ActionBarActivity {

    /*************************************
     * PRIVATE FIELDS
     *************************************/
    private MainFragment mainFragment;

    /*************************************
     * PROTECTED METHODS
     *************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Debug.enabled = true;

        /**
         * I know, that use fragments for such simple case
         * is kind of overhead, but this is standard at the moment
         * in all the apps, so I just follow the standards.
         */

        mainFragment = new MainFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_container, mainFragment)
                .addToBackStack(mainFragment.getClass().getSimpleName())
                .commit();
    }

    /*************************************
     * PUBLIC METHODS
     *************************************/
    @Override
    public void onBackPressed() {
        // close the app on pressing the back button
        finish();
    }

}
